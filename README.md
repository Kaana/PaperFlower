[<img src="https://i0.wp.com/about.kaana.io/wp-content/uploads/elementor/thumbs/Asset-2@4x-oum65rkg5c1toy0cijgx1o0q4d5iipb9bnverfabry.png?w=1170&ssl=1" width="200" />](https://gitlab.com/Kaana/PaperFlower)
## PaperFlower by Kaana
[PaperFlower by Kaana](https://gitlab.com/Kaana/PaperFlower)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GNU AGPLv3](https://choosealicense.com/licenses/agpl-3.0/)